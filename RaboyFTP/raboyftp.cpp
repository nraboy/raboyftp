// WININET FTP Commands
// Created By Nic Raboy
// February 1st 2010

/*
 * NOTE: I Don't Mind This Source Code Being Distributed.  The Only Thing I Want To See Happen If
 * Distributed, Is My Name To Remain At The Top As The Original Creator.  I Would Also Like My Name
 * To Be Referenced At The Location Of Distribution.  So If You Put This Code On Your Website, Make
 * Sure To Say That I Started The Design.
 */

/*
 * ANOTHER NOTE:  When Compiling It Is Very Important You Set The Character Set To Multi-Byte.  You 
 * Can Do This By Going To Project Properties-> Configuration Properties-> General.
 */

#include <windows.h>
#include <tchar.h>
#include <strsafe.h>
#include <wininet.h>

// Including Just The Header Is Not Good Enough.  You Need To
// Include The Library As Well.
#pragma comment(lib, "wininet.lib")

// Prepare The Functions For Exporting So DBP Can Use Them
#define MYCOMMAND __declspec ( dllexport )

// Declare And Export All Functions Being Used
// As Far As DBP Is Concerned LPSTR = String, Int = Integer, DWORD = DWORD, And Void = Nothing
MYCOMMAND int ftpConnect(LPSTR host, LPSTR username, LPSTR password);
MYCOMMAND int ftpDownload(LPSTR serverFile, LPSTR clientFile);
MYCOMMAND int ftpDownloadBinary(LPSTR serverFile, LPSTR clientFile);
MYCOMMAND int ftpUpload(LPSTR clientFile, LPSTR serverFile);
MYCOMMAND int ftpUploadBinary(LPSTR clientFile, LPSTR serverFile);
MYCOMMAND int ftpCreateDirectory(LPSTR serverDirectory);
MYCOMMAND int ftpDeleteFile(LPSTR serverFile);
MYCOMMAND int ftpDeleteDirectory(LPSTR serverDirectory);
MYCOMMAND int ftpSetDirectory(LPSTR serverDirectory);
MYCOMMAND LPSTR ftpFindFile(LPSTR serverFile, int fnum);
MYCOMMAND LPSTR ftpGetCurrentDirectory(void);
MYCOMMAND DWORD ftpFileSize(LPSTR serverFile);
MYCOMMAND int ftpDisconnect(void);
MYCOMMAND int ftpGoToRoot(void);
MYCOMMAND int ftpRenameFile(LPSTR startFile, LPSTR endFile);
MYCOMMAND LPSTR ftpAbout(void);
MYCOMMAND int ftpFileCount(void);
MYCOMMAND int ftpIsConnected(void);

HINTERNET IOHandle;	// InternetOpen Handle
HINTERNET ICHandle;	// InternetConnect Handle

// Open An Internet Connection And Then Connect To The FTP Account
int ftpConnect(LPSTR host, LPSTR username, LPSTR password)
{
	IOHandle = InternetOpen(TEXT("RaboyFTP"), INTERNET_OPEN_TYPE_DIRECT, NULL, NULL, WININET_API_FLAG_SYNC);
	if(IOHandle == NULL)
	{
		return 1; // Failed To Open The Internet
	}
	ICHandle = InternetConnect(IOHandle, host, INTERNET_DEFAULT_FTP_PORT, username, password, INTERNET_SERVICE_FTP, INTERNET_FLAG_PASSIVE, 0);
	if(ICHandle == NULL)
	{
		return 2;	// Failed To Connect To The Host
	}
	return 0;	// Connecting To FTP Succeeded
}

// Download A File From The Working Server Directory And Save It To The Local Working Directory
int ftpDownload(LPSTR serverFile, LPSTR clientFile)
{
	if(FtpGetFile(ICHandle, serverFile, clientFile, FALSE, FILE_ATTRIBUTE_NORMAL, FTP_TRANSFER_TYPE_ASCII, 0) == FALSE)
	{
		return 1;	// Failed To Download File
	}
	return 0;	// Downloaded Succeeded
}

// Download A File From The Working Server Directory In Binary And Save It To The Local Working Directory
int ftpDownloadBinary(LPSTR serverFile, LPSTR clientFile)
{
	if(FtpGetFile(ICHandle, serverFile, clientFile, FALSE, FILE_ATTRIBUTE_NORMAL, FTP_TRANSFER_TYPE_BINARY, 0) == FALSE)
	{
		return 1;	// Failed To Download File
	}
	return 0;	// Downloaded Succeeded
}

// Upload A File From The Working Local Directory And Save It To The Remote Working Directory
int ftpUpload(LPSTR clientFile, LPSTR serverFile)
{
	if(FtpPutFile(ICHandle, clientFile, serverFile, FTP_TRANSFER_TYPE_ASCII, 0) == FALSE)
	{
		return 1;	// Failed To Upload File
	}
	return 0;	// Upload Succeeded
}

// Upload A File From The Working Local Directory In Binary And Save It To The Remote Working Directory
int ftpUploadBinary(LPSTR clientFile, LPSTR serverFile)
{
	if(FtpPutFile(ICHandle, clientFile, serverFile, FTP_TRANSFER_TYPE_BINARY, 0) == FALSE)
	{
		return 1;	// Failed To Upload File
	}
	return 0;	// Upload Succeeded
}

// Create A New Directory In The Remote Server Working Directory
int ftpCreateDirectory(LPSTR serverDirectory)
{
	if(FtpCreateDirectory(ICHandle, serverDirectory) == FALSE)
	{
		return 1;	// Failed To Create Directory
	}
	return 0;	// Directory Creation Succeeded
}

// Delete A Specified File From The Remote Server Working Directory
int ftpDeleteFile(LPSTR serverFile)
{
	if(FtpDeleteFile(ICHandle, serverFile) == FALSE)
	{
		return 1;	// Failed To Delete File
	}
	return 0;	// File Deletion Succeeded
}

// Delete A Specified Directory From The Remote Server Working Directory
int ftpDeleteDirectory(LPSTR serverDirectory)
{
	if(FtpRemoveDirectory(ICHandle, serverDirectory) == FALSE)
	{
		return 1;	// Failed To Delete Directory
	}
	return 0;	// Folder Deletion Succeeded
}

// Change The Remote Server Working Directory
int ftpSetDirectory(LPSTR serverDirectory)
{
	if(FtpSetCurrentDirectory(ICHandle, serverDirectory) == FALSE)
	{
		return 1;	// Failed To Change The Directory
	}
	return 0;	// Successfully Changed The Directory
}

// Find A File On The Server With A Given File Name.  A File Number Can Be Specified
// Because You Are Allowed To Use Wildcards.  For Example Say You Type *.txt As The
// File You Want To Search For.  That Will Return All Text Files.  Say You Want
// The Second Listed Text File, You Would Enter 2 For Your File Number.  For
// Wildcard You Can Enter * For All Files Or Leave It As A Blank String For The
// First File
LPSTR ftpFindFile(LPSTR serverFile, int fnum)
{
	WIN32_FIND_DATA filedata;
	HINTERNET FFHandle = FtpFindFirstFile(ICHandle, serverFile, &filedata, INTERNET_FLAG_HYPERLINK, 0);
	// This Loop Will Iterate Through All The Files From The Search
	for(int i = 0; i < fnum; i++)
	{
		InternetFindNextFile(FFHandle, &filedata);
	}
	InternetCloseHandle(FFHandle);	// Close The Search So You Can Search Again Later
	return filedata.cFileName;
}

// Find And Return The Full Path To The Current Working Directory
LPSTR ftpGetCurrentDirectory(void)
{
	LPSTR dir = new TCHAR[MAX_PATH];
	DWORD buffer = MAX_PATH;
	FtpGetCurrentDirectory(ICHandle, dir, &buffer);
	return dir;
}

// Find A Files Size And Return It In Bytes
DWORD ftpFileSize(LPSTR serverFile)
{
	WIN32_FIND_DATA filedata;
	HINTERNET FFHandle = FtpFindFirstFile(ICHandle, serverFile, &filedata, INTERNET_FLAG_HYPERLINK, 0);
	InternetCloseHandle(FFHandle);
	return filedata.nFileSizeLow;
}

// Disconnect From The FTP Server And Then Close The Connection To The Internet
int ftpDisconnect(void)
{
	if(InternetCloseHandle(ICHandle) == FALSE)
	{
		return 1;	// Failed To Disconnect
	}
	if(InternetCloseHandle(IOHandle) == FALSE)
	{
		return 1;	// Failed To Disconnect
	}
	return 0;	// Disconnected Successfully
}

// Set The FTP Root As The Current Working Directory
int ftpGoToRoot(void)
{
	return ftpSetDirectory("/");
}

// Rename A File On The Server In The Current Working Directory
int ftpRenameFile(LPSTR startFile, LPSTR endFile)
{
	if(FtpRenameFile(ICHandle, startFile, endFile) == FALSE)
	{
		return 1;	// Failed To Rename File
	}
	return 0;	// File Rename Succeeded
}

// Just Remind Everyone Who Made And Shared The DLL And Source
LPSTR ftpAbout(void)
{
	return "Created By Nic Raboy";
}

// Determine How Many Files Are In The Current Working Directory
int ftpFileCount(void)
{
	LPSTR serverFile = "*";	// Search All Files
	int fileCount = 0;
	WIN32_FIND_DATA filedata;
	HINTERNET FFHandle = FtpFindFirstFile(ICHandle, serverFile, &filedata, INTERNET_FLAG_HYPERLINK, 0);
	while(InternetFindNextFile(FFHandle, &filedata) == TRUE)
	{
		fileCount++;
	}
	InternetCloseHandle(FFHandle);
	return fileCount-1;
}

// Check To See If Your Still Connected To The Server And Internet
int ftpIsConnected(void)
{
	if(ICHandle == NULL || IOHandle == NULL)
	{
		return 1;	// Not Connected
	}
	return 0;	// Connected
}